import React,{ useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import { Table, Input, Space, Button } from 'antd';
import Highlighter from 'react-highlight-words';
import { SearchOutlined } from '@ant-design/icons';


function MainPage(props){
  const [filteredInfo, setfiltredInfo] = useState(null)
  const [sortedInfo, setsortedInfo] = useState(null)
  const [searchText, setsearchText] = useState(null)
  const [searchedColumn, setsearchedColumn] = useState(null)
  const [searchInput, setsearchInput] = useState(null)
  const [SelectedKeys, setSelectedKeys] = useState(null)

  useEffect(() => {

  },[]);

  let handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    setfiltredInfo(filters);
    setsortedInfo(sorter);
  };

  const data = [];
  for (let i = 0; i < 51; i++) {
    data.push({
      key: i,
      name: `Edward King ${i}`,
      age: 20 + i,
      address: `London, Park Lane no. ${i}`,
    });
  }


  let getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            const searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) => {
            setSelectedKeys(e.target.value ? [e.target.value] : [])
            confirm({ closeDropdown: false });
            setsearchText(selectedKeys[1])
            setsearchedColumn(dataIndex)
            }
          }
          // onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    render: text =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  let handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setsearchText(selectedKeys[0])
    setsearchedColumn(dataIndex);
  };

  let handleReset = (clearFilters) => {
    clearFilters();
    setsearchText('');
  };


  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      onFilter: (value, record) => record.name.indexOf(value) === 0,
      ...getColumnSearchProps('name'),
    },
    {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
      sorter: {
        compare: (a, b) => a.age - b.age,
        multiple: 2,
      },
    },
    {
      title: 'Address',
      dataIndex: 'address',
      key: 'address',
      // filters: [
      //   { text: 'Park Lane no. 10', value: 'London, Park Lane no. 10' },
      //   { text: 'Park Lane no. 40', value: 'London, Park Lane no. 40'},
      // ],
    },
  ];
  
    return (
      <section>
        <Table columns={columns} dataSource={data} onChange={handleChange}>
        </Table>
      </section>
     
  )
}

export default MainPage;