import './App.css';
import MainPage from './page/index';

function App() {
  return (
    <div>
        <MainPage />
    </div>
  );
}

export default App;
